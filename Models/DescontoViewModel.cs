﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Projeto1.Models
{
    public class DescontoViewModel
    {
        public string categoria { get; set; }
        public int Desconto { get; set; }
        public string Nome { get; set; }
        public decimal Preco { get; set; }
        public decimal PrecoComDesconto { get; set; }

        public int ProdutoId { get; set; }
    }
}