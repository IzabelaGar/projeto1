﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Projeto1.Models
{
    public class Desconto
    {
        [Key]
        public int DescontoID { get; set; }
        [Display(Name = "Valor do Desconto")]
        public int ValorDesconto { get; set; }
        public virtual ICollection<Categoria> Categorias { get; set; }
        


    }
}