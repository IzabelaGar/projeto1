﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Projeto1.Models
{
    public class Produto
    {   [Key]
        public int ProdutoId { get; set; }
        [MaxLength(80, ErrorMessage = "Qtd. max. de caracter é 80")]
        [Display(Name = "Nome do Produto")]
        public string NomeProduto { get; set; }
        [Display(Name = "Preço")]
        public decimal Preco { get; set; }
        [Display(Name = "Categoria")]
        public int CategoriaId { get; set; }
        public virtual Categoria Categorias { get; set; }

    }
}