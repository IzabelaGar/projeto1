﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Projeto1.Models
{
    public class Categoria
    {
        [Key]
        public int CategoriaId { get; set; }
        [MaxLength(80, ErrorMessage = "Qtd. max. de caracter é 80")]
        [Required(ErrorMessage = "Nome da categoria não pode ficar vazio")]
        [Display(Name = "Categoria")]
        public string NomeCategoria { get; set; }
        [Display(Name = "Desconto")]
        public int? DescontoID { get; set; }
        public virtual Desconto Descontos { get; set; }
        public virtual ICollection<Produto> Produtos { get; set; }
    }
}