﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Projeto1.Models;

namespace Projeto1.Controllers
{
    public class DescontosController : Controller
    {
        private Projeto1Context db = new Projeto1Context();

        // GET: Descontos
        public ActionResult Index()
        {
            return View(db.Descontos.ToList());
        }

        // GET: Descontos/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Desconto desconto = db.Descontos.Find(id);
            if (desconto == null)
            {
                return HttpNotFound();
            }
            return View(desconto);
        }

        // GET: Descontos/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Descontos/Create
        // Para se proteger de mais ataques, ative as propriedades específicas a que você quer se conectar. Para 
        // obter mais detalhes, consulte https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "DescontoID,ValorDesconto")] Desconto desconto)
        {
            if (ModelState.IsValid)
            {
                db.Descontos.Add(desconto);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(desconto);
        }

        // GET: Descontos/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Desconto desconto = db.Descontos.Find(id);
            if (desconto == null)
            {
                return HttpNotFound();
            }
            return View(desconto);
        }

        // POST: Descontos/Edit/5
        // Para se proteger de mais ataques, ative as propriedades específicas a que você quer se conectar. Para 
        // obter mais detalhes, consulte https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "DescontoID,ValorDesconto")] Desconto desconto)
        {
            if (ModelState.IsValid)
            {
                db.Entry(desconto).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(desconto);
        }

        // GET: Descontos/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Desconto desconto = db.Descontos.Find(id);
            if (desconto == null)
            {
                return HttpNotFound();
            }
            return View(desconto);
        }

        // POST: Descontos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Desconto desconto = db.Descontos.Find(id);
            db.Descontos.Remove(desconto);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
