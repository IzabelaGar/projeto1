namespace Projeto1.Migrations
{
    using Projeto1.Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Projeto1.Models.Projeto1Context>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(Projeto1.Models.Projeto1Context context)
        {
            var categoria1 = new Categoria
            {
                CategoriaId = 1,
                NomeCategoria = "Ferramenta Eletrica"
            };

          // context.Categorias.AddOrUpdate(categoria1);
            var categoria2 = new Categoria
            {
                CategoriaId = 2,
                NomeCategoria = "Piso Cer�mica"
            };

         //context.Categorias.AddOrUpdate(categoria2);

            var categoria3 = new Categoria
            {
                CategoriaId = 3,
                NomeCategoria = "Jardinagem"
            };

          // context.Categorias.AddOrUpdate(categoria3);



          //  context.SaveChanges();
        }
    }
}
